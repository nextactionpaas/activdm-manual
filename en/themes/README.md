# Themes

Themes control the look and feel of the ActivDM landing pages, emails, forms and message screens.  A basic ActivDM installation will come pre-packaged with a number of themes which can be used 'as-is' or adapted to suit specific projects.  It is also possible to create a theme for ActivDM from scratch.